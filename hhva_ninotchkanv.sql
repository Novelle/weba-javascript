-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: h2mysql12
-- Generation Time: Mar 15, 2019 at 01:35 PM
-- Server version: 5.6.33-log
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hhva_ninotchkanv`
--

-- --------------------------------------------------------

--
-- Table structure for table `avoir`
--

CREATE TABLE `avoir` (
  `METS_ID` int(4) NOT NULL,
  `CRI_ID` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `avoir`
--

INSERT INTO `avoir` (`METS_ID`, `CRI_ID`) VALUES
(87, 4),
(87, 5),
(87, 6),
(87, 10),
(88, 3),
(88, 4),
(88, 5),
(88, 6),
(88, 10),
(89, 7),
(90, 1),
(90, 2),
(90, 7),
(90, 8),
(91, 4),
(91, 5),
(91, 6),
(91, 10),
(92, 7),
(93, 5),
(93, 7),
(93, 8),
(94, 1),
(94, 2),
(94, 5),
(94, 7),
(94, 8),
(95, 7),
(95, 8),
(96, 1),
(96, 2),
(96, 7),
(96, 8),
(96, 9),
(97, 1),
(97, 2),
(97, 5),
(97, 7),
(97, 8),
(97, 9),
(98, 1),
(98, 2),
(98, 5),
(98, 7),
(98, 9),
(99, 1),
(99, 2),
(99, 5),
(99, 7),
(99, 8),
(99, 9),
(100, 1),
(100, 2),
(100, 5),
(100, 7),
(100, 8),
(101, 7),
(102, 1),
(102, 2),
(102, 7),
(102, 8),
(102, 9),
(103, 3),
(103, 4),
(103, 5),
(103, 7),
(104, 5),
(104, 7),
(105, 4),
(105, 5),
(105, 7),
(105, 8),
(105, 9),
(106, 4),
(106, 5),
(106, 7),
(106, 8),
(107, 5),
(107, 7),
(107, 8),
(108, 5),
(108, 7),
(108, 8),
(109, 7),
(110, 4),
(110, 5),
(110, 7),
(111, 3),
(111, 4),
(111, 5),
(111, 7),
(111, 8),
(112, 7),
(112, 9),
(113, 7),
(113, 8),
(113, 9),
(114, 7),
(115, 7),
(115, 8),
(116, 1),
(116, 2),
(116, 7),
(116, 8),
(116, 9),
(117, 5),
(117, 7),
(117, 8),
(117, 9),
(118, 3),
(118, 4),
(118, 5),
(118, 7),
(118, 8),
(119, 1),
(119, 2),
(119, 7),
(119, 9),
(120, 1),
(120, 2),
(120, 7),
(120, 9),
(121, 4),
(121, 5),
(121, 7),
(121, 9),
(122, 3),
(122, 4),
(122, 5),
(122, 7),
(122, 8),
(123, 1),
(123, 2),
(123, 3),
(123, 4),
(123, 5),
(123, 7),
(123, 8),
(123, 9),
(124, 3),
(124, 4),
(124, 5),
(124, 7),
(125, 1),
(125, 2),
(125, 7),
(125, 8),
(126, 4),
(126, 5),
(126, 7),
(126, 9),
(127, 3),
(127, 4),
(127, 5),
(127, 7),
(127, 8),
(128, 3),
(128, 4),
(128, 5),
(128, 7),
(128, 9),
(129, 4),
(129, 5),
(129, 7),
(129, 8),
(129, 9),
(130, 1),
(130, 2),
(130, 5),
(130, 7),
(130, 8),
(130, 9),
(131, 3),
(131, 4),
(131, 5),
(131, 7),
(131, 8),
(131, 9),
(132, 7),
(132, 8),
(133, 7),
(134, 1),
(134, 2),
(134, 3),
(134, 4),
(134, 5),
(134, 7),
(134, 8),
(134, 9),
(135, 1),
(135, 2),
(135, 5),
(135, 7),
(135, 8),
(135, 9),
(136, 5),
(136, 7),
(136, 8),
(136, 9),
(137, 1),
(137, 2),
(137, 5),
(137, 7),
(137, 8),
(137, 9),
(139, 7),
(139, 8),
(140, 1),
(140, 2),
(140, 7),
(140, 8),
(140, 9),
(141, 4),
(141, 7),
(141, 8),
(141, 9),
(142, 7),
(142, 9),
(143, 1),
(143, 2),
(143, 3),
(143, 4),
(143, 5),
(143, 7),
(143, 8),
(143, 9),
(144, 1),
(144, 2),
(144, 3),
(144, 4),
(144, 5),
(144, 7),
(144, 8),
(144, 9),
(145, 3),
(145, 4),
(145, 5),
(145, 7),
(145, 8),
(147, 3),
(147, 4),
(147, 5),
(147, 7),
(147, 8),
(148, 3),
(148, 4),
(148, 5),
(148, 7),
(149, 7),
(149, 8),
(150, 7),
(150, 8),
(151, 1),
(151, 2),
(151, 7),
(151, 8),
(151, 9),
(152, 5),
(152, 7),
(152, 8),
(153, 4),
(153, 5),
(153, 6),
(153, 10),
(154, 3),
(154, 4),
(154, 5),
(154, 6),
(154, 10),
(155, 4),
(155, 5),
(155, 6),
(155, 8),
(155, 10),
(156, 4),
(156, 5),
(156, 6),
(156, 10),
(157, 1),
(157, 2),
(157, 3),
(157, 4),
(157, 5),
(157, 7),
(157, 8),
(157, 9),
(158, 7),
(158, 8),
(158, 9),
(159, 5),
(159, 7),
(160, 3),
(160, 4),
(160, 5),
(160, 7),
(160, 8),
(161, 3),
(161, 4),
(161, 5),
(161, 7),
(161, 8),
(162, 1),
(162, 2),
(162, 7),
(162, 9),
(163, 1),
(163, 2),
(163, 7),
(163, 9),
(181, 1),
(181, 2),
(181, 3),
(181, 4),
(181, 5),
(181, 7),
(181, 8);

-- --------------------------------------------------------

--
-- Table structure for table `cepage`
--

CREATE TABLE `cepage` (
  `CEP_ID` int(4) NOT NULL,
  `CEP_NOM` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cepage`
--

INSERT INTO `cepage` (`CEP_ID`, `CEP_NOM`) VALUES
(1, 'sauvignon'),
(2, 'merlot'),
(3, 'traminer'),
(4, 'viognier'),
(5, 'pinotnoir'),
(6, 'pinotblanc'),
(7, 'pinotgris'),
(8, 'chardonnay'),
(9, 'cabernet'),
(10, 'chasselas'),
(11, 'gamay'),
(12, 'muscat'),
(13, 'syrah'),
(14, 'petitmanseng'),
(15, 'petitearvine'),
(16, 'savagninblancrose');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `CLI_ID` int(4) NOT NULL,
  `CLI_NOM` varchar(20) DEFAULT NULL,
  `CLI_PRENOM` varchar(20) DEFAULT NULL,
  `CLI_MAJEUR` tinyint(1) DEFAULT NULL,
  `CLI_NEWSLETTER` tinyint(1) DEFAULT NULL,
  `CLI_ETATCLIENT` tinyint(1) DEFAULT NULL,
  `CLI_LOGIN_MAIL` varchar(100) DEFAULT NULL,
  `CLI_MDP` varchar(250) DEFAULT NULL,
  `CLI_ISADMIN` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`CLI_ID`, `CLI_NOM`, `CLI_PRENOM`, `CLI_MAJEUR`, `CLI_NEWSLETTER`, `CLI_ETATCLIENT`, `CLI_LOGIN_MAIL`, `CLI_MDP`, `CLI_ISADMIN`) VALUES
(10, 'ADMIN', 'Ninotchka', 1, 0, 1, 'novelle.ninotchka@gmail.com', 'b21da59045265d9cc91543057359001dba3a5269', 1),
(22, 'CLIENTADMIN', 'Anne', 1, 0, 1, 'legrandclos2017@gmail.com', 'b21da59045265d9cc91543057359001dba3a5269', 0),
(30, 'GOUVEIA', 'Micaela', 1, 0, 1, 'micaela.gouveia@gmail.com', 'b21da59045265d9cc91543057359001dba3a5269', 0),
(31, 'KARA', 'Nisa', 1, 0, 1, 'nisa.kara@gmail.com', '87cd6796d43164b40e4f086d3419da0f0300e474', 0),
(37, 'BLEU', 'Jean Pierre', 1, 0, 0, 'pierre.bleu@gmail.com', 'c686e33189a97a0fc418ff4c286d66f7621bcc05', 0),
(38, 'MONNIER', 'Claude', 1, 0, 1, 'claude.monnier@gmail.com', '', 0),
(39, 'MONNIER', 'Claude', 1, 0, 1, 'mon.cla@gmail.com', '', 0),
(40, 'MACARON', 'June', 1, 0, 1, 'macaron.june@gmail.com', '', 0),
(41, 'MICAELA', 'Gouveia', 1, 0, 1, 'novelle.ninotchka@hotmail.com', '2069342cac945b8023b003e61f73b98ab788dbd0', 0),
(42, 'MICAELA', 'Gouveia', 1, 0, 1, 'micaela.gouveia@hotmail.com', '2069342cac945b8023b003e61f73b98ab788dbd0', 0),
(43, 'CID', 'Laura', 1, 0, 1, 'laura.cid@gmail.com', '', 0),
(44, 'MARTINEZ', 'Laura', 1, 0, 1, 'laura.cm@gmail.com', '', 0),
(45, '<STRONG>TCHOIN</STRO', 'Laura', 1, 0, 1, 'laura.cid@test.com', '2069342cac945b8023b003e61f73b98ab788dbd0', 0),
(46, '<U>TEST', 'Test', 1, 0, 1, 'test@test.com', '0993d57952a536720aaacf664fad2fcc36e3b68b', 0),
(47, 'KARA', 'Nisa', 1, 0, 1, 'nisakara@gmail.com', '88a4b5292b6eeaa5c843a3cb782746c409f756c2', 0),
(48, 'KARA', 'Nisa', 1, 0, 1, 'nisa.kara@hotmail.com', '696a251cf8941e8fdaf822448be2623d5ec25891', 0),
(49, 'KARA', 'Nisa', 1, 0, 1, 'nisa.kara@gmail.ch', '696a251cf8941e8fdaf822448be2623d5ec25891', 0),
(50, 'KARA', 'Nisa', 1, 0, 1, 'nisa.karaa@gmail.ch', '88a4b5292b6eeaa5c843a3cb782746c409f756c2', 0),
(51, 'KARA', 'Nisa', 1, 0, 1, 'nisa-kara@gmail.ch', '88a4b5292b6eeaa5c843a3cb782746c409f756c2', 0),
(52, 'BOB', 'Bob', 1, 0, 1, 'bob@gmail.com', '88a4b5292b6eeaa5c843a3cb782746c409f756c2', 0),
(53, 'TEST', 'Test', 1, 0, 1, 'test.test@gmail.com', 'c186b8a47e28524e38fc1cfc1c5d596703af237b', 0),
(54, 'NOM', 'Prenom', 1, 0, 1, 'nom@prenom.com', '88a4b5292b6eeaa5c843a3cb782746c409f756c2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contenir`
--

CREATE TABLE `contenir` (
  `VIN_ID` int(4) NOT NULL,
  `CEP_ID` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contenir`
--

INSERT INTO `contenir` (`VIN_ID`, `CEP_ID`) VALUES
(2, 1),
(2, 4),
(2, 12),
(2, 16),
(3, 14),
(3, 15),
(5, 7),
(6, 8),
(7, 4),
(8, 1),
(9, 16),
(10, 16),
(11, 12),
(12, 1),
(12, 14),
(12, 15),
(13, 11),
(14, 5),
(15, 5),
(15, 11),
(15, 13),
(16, 11),
(17, 5),
(18, 11),
(19, 5),
(20, 2),
(21, 13),
(22, 9),
(24, 5),
(24, 13),
(25, 5),
(26, 5);

-- --------------------------------------------------------

--
-- Table structure for table `criteremets`
--

CREATE TABLE `criteremets` (
  `CRI_ID` int(4) NOT NULL,
  `CRI_NOM` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `criteremets`
--

INSERT INTO `criteremets` (`CRI_ID`, `CRI_NOM`) VALUES
(1, 'sanslactose'),
(2, 'sansgluten'),
(3, 'vegan'),
(4, 'vegetarien'),
(5, 'pescetarien'),
(6, 'sucre'),
(7, 'sale'),
(8, 'aperitif'),
(9, 'atable'),
(10, 'dessert');

-- --------------------------------------------------------

--
-- Table structure for table `mets`
--

CREATE TABLE `mets` (
  `METS_ID` int(4) NOT NULL,
  `METS_NOM` varchar(50) DEFAULT NULL,
  `METS_NBRECHERCHE` int(4) DEFAULT NULL,
  `METS_LIENIMAGE` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mets`
--

INSERT INTO `mets` (`METS_ID`, `METS_NOM`, `METS_NBRECHERCHE`, `METS_LIENIMAGE`) VALUES
(86, 'risotto', NULL, 'mets/risotto.jpeg'),
(87, 'cheesecake mangue', NULL, 'mets/cheescakemangue.jpeg'),
(88, 'salade orange', NULL, 'mets/saladeorange.jpeg'),
(89, 'chevreuille', NULL, 'mets/chevreuille.jpeg'),
(90, 'foie gras', NULL, 'mets/foiegras.jpeg'),
(91, 'frangipane', NULL, 'mets/frangipane.jpeg'),
(92, 'pintade', NULL, 'mets/pintade.jpeg'),
(93, 'saumon', NULL, 'mets/saumon.jpeg'),
(94, 'saumon aneth', NULL, 'mets/saumonaneth.jpeg'),
(95, 'caille', NULL, 'mets/caille.jpeg'),
(96, 'boeuf coulis framboise', NULL, 'mets/boeufcouliframboise.jpeg'),
(97, 'crustacés frits', NULL, 'mets/courgefarcie.jpeg'),
(98, 'sole', NULL, 'mets/sole.jpeg'),
(99, 'saumon mariné', NULL, 'mets/saumonmariner.jpeg'),
(100, 'huitre', NULL, 'mets/huitre.jpeg'),
(101, 'pigeon', NULL, 'mets/pigeon.jpeg'),
(102, 'chasse', NULL, 'mets/chasse.jpeg'),
(103, 'courge farcie', NULL, 'mets/courgefarcie.jpeg'),
(104, 'saumon papillote', NULL, 'mets/saumonpapillote.jpeg'),
(105, 'quiche', NULL, 'mets/quiche.jpeg'),
(106, 'lasagne végétarienne', NULL, 'mets/lasagnevegetarienne.jpeg'),
(107, 'terrine poisson', NULL, 'mets/terrinepoisson.jpeg'),
(108, 'truite papillote', NULL, 'mets/truitepapillote.jpeg'),
(109, 'gratin pomme de terre', NULL, 'mets/gratinpommedeterre.jpeg'),
(110, 'fondue', NULL, 'mets/fondue.jpeg'),
(111, 'lentille', NULL, 'mets/lentille.jpeg'),
(112, 'spaghetti bolognaise', NULL, 'mets/spaghettibolo.jpeg'),
(113, 'lasagne', NULL, 'mets/lasagne.jpeg'),
(114, 'couscous', NULL, 'mets/couscous_tajine.jpeg'),
(115, 'tacos', NULL, 'mets/tacos.jpeg'),
(116, 'poivrons farcis', NULL, 'mets/poivronfarcis.jpeg'),
(117, 'sushi', NULL, 'mets/sushi.jpeg'),
(118, 'frites de patates douces', NULL, 'mets/fritepatatedouce.jpeg'),
(119, 'dinde', NULL, 'mets/dinde.jpeg'),
(120, 'magret de canard', NULL, 'mets/magret.jpeg'),
(121, 'ravioles', NULL, 'mets/raviole.jpeg'),
(122, 'artichaut', NULL, 'mets/artichaut.jpeg'),
(123, 'asperges', NULL, 'mets/asperge.jpeg'),
(124, 'spaghetti pesto', NULL, 'mets/pesto.jpeg'),
(125, 'brochette', NULL, 'mets/brochetteviande.jpeg'),
(126, 'raclette', NULL, 'mets/raclette.jpeg'),
(127, 'frites', NULL, 'mets/frite.jpeg'),
(128, 'légumes sautés', NULL, 'mets/legumesaute.jpeg'),
(129, 'vol au vent', NULL, 'mets/volauvent.jpeg'),
(130, 'saumon grillé', NULL, 'mets/saumongrille.jpeg'),
(131, 'poêlé de champignon', NULL, 'mets/poelechampignon.jpeg'),
(132, 'wrap', NULL, 'mets/wrap.jpeg'),
(133, 'paella', NULL, 'mets/paella.jpeg'),
(134, 'ratatouille', NULL, 'mets/ratatouille.jpeg'),
(135, 'salade de crevettes', NULL, 'mets/saladecrevette.jpeg'),
(136, 'crevette panée', NULL, 'mets/crevettepane.jpeg'),
(137, 'moules', NULL, 'mets/moule.jpeg'),
(139, 'terrine chasse', NULL, 'mets/terrinechasse.jpeg'),
(140, 'aubergine farcie', NULL, 'mets/auberginefarcievege.jpeg'),
(141, 'pizza', NULL, 'mets/pizza.jpeg'),
(142, 'tagliatelle carbonara', NULL, 'mets/tagliatellecarbonara.jpeg'),
(143, 'taboulé', NULL, 'mets/taboule.jpeg'),
(144, 'aubergine grillée', NULL, 'mets/auberginegrillee.jpeg'),
(145, 'brochette de légume', NULL, 'mets/brochettelegume.jpeg'),
(146, 'poivron farcis viande', NULL, 'mets/courgefarcie.jpeg'),
(147, 'courgette farcie végétarienne', NULL, 'mets/courgettefarcievegetarien.jpeg'),
(148, 'tagliatelle aux truffes', NULL, 'mets/tagliatelletruffe.jpeg'),
(149, 'sashimi', NULL, 'mets/sashimi.jpeg'),
(150, 'nems', NULL, 'mets/nems.jpeg'),
(151, 'courgettes farcies à la viande', NULL, 'mets/courgettefarcieviande.jpeg'),
(152, 'crevettes', NULL, 'mets/crevettes.jpeg'),
(153, 'crème brûlée', NULL, 'mets/creme_brulee.jpeg'),
(154, 'salade de fruits', NULL, 'mets/saladefruit.jpeg'),
(155, 'patisserie orientale', NULL, 'mets/patisserieorientale.jpeg'),
(156, 'tarte tatin', NULL, 'mets/tartetatin.jpeg'),
(157, 'poivrons farcis végétariens', NULL, 'mets/poivronfarcivege.jpeg'),
(158, 'hamburger', NULL, 'mets/hamburger.jpeg'),
(159, 'spaghetti crevettes', NULL, 'mets/spaghetticrevette.jpeg'),
(160, 'samossa aux légumes', NULL, 'mets/samossa.jpeg'),
(161, 'gratin de légumes', NULL, 'mets/gratinlegume.jpeg'),
(162, 'poulet sauté', NULL, 'mets/pouletsaute.jpeg'),
(163, 'poulet au curry', NULL, 'mets/pouletcurry.jpeg'),
(181, 'tapenade', NULL, 'mets/taponade.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `parametre`
--

CREATE TABLE `parametre` (
  `PAR_NOCLIENTBIDON` int(2) DEFAULT NULL,
  `PAR_NOCLIENTADMIN` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suggestion`
--

CREATE TABLE `suggestion` (
  `SUG_ID` int(4) NOT NULL,
  `CLI_ID` int(4) NOT NULL,
  `VIN_ID` int(4) NOT NULL,
  `METS_ID` int(4) NOT NULL,
  `SUG_ETATSUGGESTION` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suggestion`
--

INSERT INTO `suggestion` (`SUG_ID`, `CLI_ID`, `VIN_ID`, `METS_ID`, `SUG_ETATSUGGESTION`) VALUES
(61, 22, 2, 86, 1),
(62, 22, 2, 87, 1),
(64, 22, 2, 88, 1),
(65, 22, 2, 89, 1),
(66, 22, 25, 90, 1),
(67, 22, 25, 91, 1),
(68, 22, 25, 92, 1),
(69, 22, 25, 93, 1),
(70, 22, 26, 94, 1),
(71, 22, 26, 95, 1),
(72, 22, 26, 96, 1),
(73, 22, 27, 97, 1),
(74, 22, 27, 98, 1),
(75, 22, 27, 99, 1),
(76, 22, 27, 100, 1),
(77, 22, 22, 101, 1),
(78, 22, 22, 102, 1),
(79, 22, 6, 103, 1),
(80, 22, 6, 104, 1),
(81, 22, 6, 105, 1),
(82, 22, 6, 106, 1),
(83, 22, 1, 107, 1),
(84, 22, 1, 108, 1),
(85, 22, 1, 109, 1),
(86, 22, 1, 110, 1),
(87, 22, 15, 111, 1),
(88, 22, 15, 112, 1),
(89, 22, 15, 113, 1),
(90, 22, 15, 114, 1),
(91, 22, 15, 115, 1),
(92, 22, 15, 116, 1),
(93, 22, 7, 117, 1),
(94, 22, 20, 118, 1),
(95, 22, 20, 119, 1),
(96, 22, 20, 120, 1),
(97, 22, 20, 121, 1),
(98, 22, 11, 122, 1),
(99, 22, 11, 123, 1),
(100, 22, 11, 124, 1),
(101, 22, 4, 125, 1),
(102, 22, 4, 126, 1),
(103, 22, 4, 127, 1),
(104, 22, 4, 128, 1),
(105, 22, 5, 129, 1),
(106, 22, 5, 130, 1),
(107, 22, 5, 131, 1),
(108, 22, 13, 132, 1),
(109, 22, 13, 133, 1),
(110, 22, 13, 134, 1),
(111, 22, 14, 135, 1),
(112, 22, 14, 136, 1),
(113, 22, 14, 137, 1),
(115, 22, 16, 139, 1),
(116, 22, 16, 140, 1),
(117, 22, 16, 141, 1),
(118, 22, 16, 142, 1),
(119, 22, 17, 143, 1),
(120, 22, 17, 144, 1),
(121, 22, 17, 145, 1),
(122, 22, 17, 146, 1),
(123, 22, 8, 147, 1),
(124, 22, 8, 148, 1),
(125, 22, 8, 149, 1),
(126, 22, 8, 150, 1),
(127, 22, 9, 151, 1),
(128, 22, 9, 152, 1),
(129, 22, 12, 153, 1),
(130, 22, 12, 154, 1),
(131, 22, 12, 155, 1),
(132, 22, 12, 156, 1),
(133, 22, 21, 157, 1),
(134, 22, 21, 158, 1),
(135, 22, 10, 159, 1),
(136, 22, 10, 160, 1),
(137, 22, 10, 161, 1),
(138, 22, 10, 162, 1),
(139, 22, 10, 163, 1),
(165, 22, 26, 90, 1),
(166, 22, 6, 86, 1),
(167, 22, 7, 100, 1),
(168, 22, 7, 93, 1),
(169, 22, 20, 95, 1),
(170, 22, 4, 110, 1),
(171, 22, 4, 101, 1),
(172, 22, 13, 112, 1),
(173, 22, 13, 119, 1),
(174, 22, 12, 148, 1),
(175, 22, 12, 90, 1),
(176, 22, 21, 113, 1),
(177, 22, 21, 125, 1),
(180, 22, 3, 100, 1),
(196, 22, 15, 181, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vin`
--

CREATE TABLE `vin` (
  `VIN_ID` int(4) NOT NULL,
  `VIN_NOM` varchar(20) DEFAULT NULL,
  `VIN_MILLESIME` varchar(40) DEFAULT NULL,
  `VIN_COULEUR` varchar(20) DEFAULT NULL,
  `VIN_COLLECTION` varchar(20) DEFAULT NULL,
  `VIN_POTENTIELGARDE` varchar(20) DEFAULT NULL,
  `VIN_PRIX` decimal(10,2) DEFAULT NULL,
  `VIN_TEMPERATURESERVICE` varchar(20) DEFAULT NULL,
  `VIN_FLACONNAGE` varchar(20) DEFAULT NULL,
  `VIN_DEGREALCOOL` decimal(10,2) DEFAULT NULL,
  `VIN_LIENIMAGE` varchar(250) DEFAULT NULL,
  `VIN_NBRECHERCHE` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vin`
--

INSERT INTO `vin` (`VIN_ID`, `VIN_NOM`, `VIN_MILLESIME`, `VIN_COULEUR`, `VIN_COLLECTION`, `VIN_POTENTIELGARDE`, `VIN_PRIX`, `VIN_TEMPERATURESERVICE`, `VIN_FLACONNAGE`, `VIN_DEGREALCOOL`, `VIN_LIENIMAGE`, `VIN_NBRECHERCHE`) VALUES
(1, 'Blanche', 'juillet 2017', 'blanc', 'Empreinte', '5-8ans', '16.00', '10-12', '75cl', '14.00', 'imageCollection/EmpreinteBlanche.JPG', NULL),
(2, 'Aromatique', 'juillet 2017', 'blanc', 'Empreinte', '8-10ans', '16.00', '10-12', '75cl', '14.00', 'imageCollection/EmpreinteAromatique.JPG', NULL),
(3, 'Marine', 'avril 2018', 'blanc', 'Empreinte', '10-12ans', '25.00', '10', '75cl', '14.00', 'imageCollection/Empreinte-Marine.jpg', NULL),
(4, 'Pinot Blanc', '2016', 'blanc', 'Iconique', '10-12ans', '17.00', '10-12', '75cl', '14.00', 'imageCollection/IconiquePinotBlanc.JPG', NULL),
(5, 'Pinot Gris', '2016', 'blanc', 'Iconique', '10-12ans', '17.00', '10-12', '75cl', '14.50', 'imageCollection/IconiquePinotGris.JPG', NULL),
(6, 'Chardonnay', '2016', 'blanc', 'Iconique', '10-12ans', '25.00', '10-12', '75cl', '14.00', 'imageCollection/IconiqueChardonnay.JPG', NULL),
(7, 'Viognier', '2015', 'blanc', 'Iconique', '10-12ans', '25.00', '10-12', '75cl', '14.00', 'imageCollection/IconiqueViognier.JPG', NULL),
(8, 'Sauvignon Blanc', '2016', 'blanc', 'Empreinte', '10-12ans', '25.00', '10-12', '75cl', '14.00', 'imageCollection/IconiqueSauvignon.JPG', NULL),
(9, 'Savagnin Blanc', '2016', 'blanc', 'Iconique', '10-12ans', '25.00', '10-12', '75cll', '14.00', 'imageCollection/IconiqueSavagnin.JPG', NULL),
(10, 'Traminer', '2016', 'blanc', 'Iconique', '10-12ans', '25.00', '10-12', '75cl', '14.00', 'imageCollection/IconiqueTraminer.JPG', NULL),
(11, 'Muscat', '2016', 'blanc', 'Iconique', '10-12ans', '25.00', '10-12', '75cl', '13.00', 'imageCollection/IconiqueMuscat.JPG', NULL),
(12, 'Solera', 'mis en bouteille en 2002', 'blanc', 'Empreinte', '15-20 ans', '43.00', '6-8 °C', '50 cl', '11.50', 'imageCollection/EmpreinteSolera.jpg', NULL),
(13, 'Gamay', '2016', 'rose', 'Rose', '8-10ans', '15.00', '14-18', '75cl', '13.50', 'imageCollection/RoseGamay.JPG', NULL),
(14, 'Pinot Noir', '2015', 'rose', 'Rose', '8-10ans', '25.00', '16-18', '75cl', '14.00', 'imageCollection/RosePinotNoir.JPG', NULL),
(15, 'Noire', 'juillet 2017', 'blanc', 'Empreinte', '10', '20.00', '10', '75cl', '14.00', 'imageCollection/EmpreinteNoire.JPG', NULL),
(16, 'Gamay', '2017', 'rouge', 'Rouge', '3-5ans', '15.00', '6-10', '75cl', '13.00', 'imageCollection/RougeGamay.JPG', NULL),
(17, 'Pinot Noir', '2016', 'rouge', 'Rouge', '3-5ans', '16.00', '6-10', '75cl', '12.50', 'imageCollection/RougePinotNoir.JPG', NULL),
(18, 'Gamay', '2016', 'rouge', 'Iconique', '3-6ans', '20.00', '12-14', '75cl', '13.50', 'imageCollection/IconiqueGamay.JPG', NULL),
(19, 'Pinot Noir', '2016', 'rouge', 'Iconique', '3-6ans', '21.00', '12-16', '75cl', '13.00', 'imageCollection/IconiquePinotNoir.JPG', NULL),
(20, 'Merlot', '2015', 'rouge', 'Iconique', '15-20ans', '35.00', '16-18', '75cl', '14.00', 'imageCollection/IconiqueMerlot.JPG', NULL),
(21, 'Syrah', '2015', 'rouge', 'Iconique', '15-20ans', '35.00', '16-18', '75cl', '14.00', 'imageCollection/IconiqueSyrah.JPG', NULL),
(22, 'Cabernet', '2015', 'rouge', 'Iconique', '15-20ans', '35.00', '16-18', '75cl', '14.00', 'imageCollection/IconiqueCabernet.JPG', NULL),
(23, 'I', 'juillet 2018', 'rouge', 'Point Final', '20 ans', '70.00', '16-18', '75cl', '14.00', 'imageCollection/PointFinalI.JPG', NULL),
(24, 'II', 'juillet 2018', 'rouge', 'Point Final', '20 ans', '80.00', '16-18', '75cl', '14.00', 'imageCollection/PointFinalII.JPG', NULL),
(25, 'blanc de noirs', '2008', 'effervescent', 'Brut', '10-15ans', '108.00', '9-12', '75cl', '13.00', 'imageCollection/BrutPinotNoir.JPG', NULL),
(26, 'Rose de noirs', '2008', 'effervescent', 'Brut', '10-15', '108.00', '9-12', '75cl', '13.00', 'imageCollection/BrutPinotNoirRose.JPG', NULL),
(27, 'Zéro blanc de noirs', '2008', 'effervescent', 'Brut', '10-15', '108.00', '9-12', '75cl', '13.00', 'imageCollection/BrutPinotNoirZero.JPG', NULL),
(59, 'III', '2012', 'blanc', 'Empreinte', '10-15', '80.00', '9', '75 cl', '13.00', 'imageCollection/', NULL),
(60, 'III', '2012', 'blanc', 'Empreinte', '10-15', '80.00', '9', '75 cl', '13.00', 'imageCollection/', NULL),
(61, 'III', '2012', 'blanc', 'Empreinte', '10-15', '80.00', '9', '75 cl', '13.00', 'imageCollection/', NULL),
(62, 'III', '2012', 'blanc', 'Empreinte', '10-15', '80.00', '9', '75 cl', '13.00', 'imageCollection/', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `avoir`
--
ALTER TABLE `avoir`
  ADD PRIMARY KEY (`METS_ID`,`CRI_ID`),
  ADD KEY `I_FK_AVOIR_METS` (`METS_ID`),
  ADD KEY `I_FK_AVOIR_CRITEREMETS` (`CRI_ID`);

--
-- Indexes for table `cepage`
--
ALTER TABLE `cepage`
  ADD PRIMARY KEY (`CEP_ID`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`CLI_ID`);

--
-- Indexes for table `contenir`
--
ALTER TABLE `contenir`
  ADD PRIMARY KEY (`VIN_ID`,`CEP_ID`),
  ADD KEY `I_FK_CONTENIR_VIN` (`VIN_ID`),
  ADD KEY `I_FK_CONTENIR_CEPAGE` (`CEP_ID`);

--
-- Indexes for table `criteremets`
--
ALTER TABLE `criteremets`
  ADD PRIMARY KEY (`CRI_ID`);

--
-- Indexes for table `mets`
--
ALTER TABLE `mets`
  ADD PRIMARY KEY (`METS_ID`);

--
-- Indexes for table `suggestion`
--
ALTER TABLE `suggestion`
  ADD PRIMARY KEY (`SUG_ID`),
  ADD KEY `I_FK_SUGGESTION_CLIENT` (`CLI_ID`),
  ADD KEY `I_FK_SUGGESTION_VIN` (`VIN_ID`),
  ADD KEY `I_FK_SUGGESTION_METS` (`METS_ID`);

--
-- Indexes for table `vin`
--
ALTER TABLE `vin`
  ADD PRIMARY KEY (`VIN_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cepage`
--
ALTER TABLE `cepage`
  MODIFY `CEP_ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `CLI_ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `criteremets`
--
ALTER TABLE `criteremets`
  MODIFY `CRI_ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `mets`
--
ALTER TABLE `mets`
  MODIFY `METS_ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;
--
-- AUTO_INCREMENT for table `suggestion`
--
ALTER TABLE `suggestion`
  MODIFY `SUG_ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;
--
-- AUTO_INCREMENT for table `vin`
--
ALTER TABLE `vin`
  MODIFY `VIN_ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `avoir`
--
ALTER TABLE `avoir`
  ADD CONSTRAINT `AVOIR_ibfk_1` FOREIGN KEY (`METS_ID`) REFERENCES `mets` (`METS_ID`),
  ADD CONSTRAINT `AVOIR_ibfk_2` FOREIGN KEY (`CRI_ID`) REFERENCES `criteremets` (`CRI_ID`);

--
-- Constraints for table `contenir`
--
ALTER TABLE `contenir`
  ADD CONSTRAINT `CONTENIR_ibfk_1` FOREIGN KEY (`VIN_ID`) REFERENCES `vin` (`VIN_ID`),
  ADD CONSTRAINT `CONTENIR_ibfk_2` FOREIGN KEY (`CEP_ID`) REFERENCES `cepage` (`CEP_ID`);

--
-- Constraints for table `suggestion`
--
ALTER TABLE `suggestion`
  ADD CONSTRAINT `SUGGESTION_ibfk_1` FOREIGN KEY (`CLI_ID`) REFERENCES `client` (`CLI_ID`),
  ADD CONSTRAINT `SUGGESTION_ibfk_2` FOREIGN KEY (`VIN_ID`) REFERENCES `vin` (`VIN_ID`),
  ADD CONSTRAINT `SUGGESTION_ibfk_3` FOREIGN KEY (`METS_ID`) REFERENCES `mets` (`METS_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
