<?php
require_once('includes/fonctionsBD.php');
require_once ('includes/check.php');
session_start();

if($_SESSION['CLI_ISADMIN'] == 1)
{
  	session_destroy();
    header('Location: seConnecter.php');
    exit();
  	
}
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>

    <meta charset="UTF-8">
    <title>apresLogin</title>
    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="style.css" type="text/css" rel="stylesheet" />
</head>

<body class="black">
<header>
    <nav class="black">

        <div class="row">   </div>

        <div class="container">
            <div class="nav-wrapper">

                <a href="afterLogin.php" class="brand-logo">
                    <img src="imageCollection/logo.jpg" class="responsive-img"  width=100 height=100  alt="Photo du logo"/>
                </a>
                <a href="#" data-target="mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="RechercheGenerale.html">Recherche Générale</a></li>
                    <li><a href="rechercheVin.php">Recherche d'un vin</a></li>
                    <li><a href="rechercheMets.php">Recherche d'un mets</a></li>
				    <li><a href="suggestionAccord.php">Suggestion</a></li>
                    <li><a href="deconnexion.php">Deconnexion</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <ul class="sidenav" id="mobile">
        <li><a href="RechercheGenerale.html">Recherche Générale</a></li>
        <li><a href="rechercheVin.php">Recherche d'un vin</a></li>
        <li><a href="rechercheMets.php">Recherche d'un mets</a></li>
	  	<li><a href="suggestionAccord.php">Suggestion</a></li>
        <li><a href="deconnexion.php">Deconnexion</a></li>
    </ul>
</header>
<br/><br/><br/>

<div class="container">
    <div>
	  <?php
        try {
            $bdd = new PDO("mysql:host=hhva.myd.infomaniak.com;dbname=hhva_ninotchkanv", "hhva_ninotchkanv", "dt14q9PS3v");
			$bdd->query("SET NAMES 'utf8'");
			  
		  
        } catch (PDOException $e) {
            die('Erreur : ' . $e->getMessage());
        }
        ?>
        <h1 class="center-align white-text">Bienvenue
		  
		  <?php
        
		if (isset($_POST['mail']))
			{
			$query = $bdd->prepare('SELECT CLI_NOM, CLI_PRENOM FROM client WHERE CLI_LOGIN_MAIL = ?');
			$query->bindParam(1, $_POST['mail'], PDO::PARAM_STR);
			$query->execute();
			
		  if($query->rowCount()){
				while ($boucle =$query->fetch())
				{
					?>
	
					<span class=" amber-text lighten-5 "><?php echo $boucle['CLI_PRENOM'] . " " . $boucle['CLI_NOM']; ?></span>
	
					<?php
				}
			}
			}
        ?>
        </h1>

    </div>
    <br/>
    <div class="center-align">
        <img src="imageCollection/AdobeStock_31147963.jpeg" class="responsive-img"  width="800"  align="center"  alt="Photo d'un accord" />
    </div>

<br/>
    <p class="center-align">
	  <a href="suggestionAccord.php" class="center-align amber-text lighten-5"> Envie de faire une suggestion ?</a>
        <br /> <br />
        <a href="gererClients.php" class="center-align amber-text lighten-5"> Besoin de modifier vos informations ?</a>
    </p>

<!--    <div class="left white-text">-->
<!--        <li><a href="RechercheGenerale.html">Recherche Générale</a></li>-->
<!--        <li><a href="rechercheVIn.php">Recherche d'un vin</a></li>-->
<!--        <li><a href="recherhceMets.php">Recherche d'un mets</a></li>-->
<!--        <li><a href="deconnexion.php">Déconnexion</a></li>-->
<!--    </div>-->



</div>
    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="js/materialize.js"></script>
    <script src="js/init.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('select').formSelect();
        });
    </script>
</body>
</html>
