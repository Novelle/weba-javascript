<?php session_start(); 

if (isset($_POST['submit'])) {

try {

	$bdd = new PDO("mysql:host=hhva.myd.infomaniak.com;dbname=hhva_ninotchkanv", "hhva_ninotchkanv", "dt14q9PS3v");

	$bdd->query("SET NAMES 'utf8'");
	

	$bdd->query("INSERT INTO mets (METS_NOM, METS_LIENIMAGE) VALUES ('$_POST[nom]', '$_POST[img]')");


    $lastIdMets = $bdd->lastInsertId();



	//REGIME ALIMENTAIRE//
	if (isset($_POST['sanslactose']))
    {
        $bdd->query("INSERT INTO avoir (METS_ID, CRI_ID) VALUES ('$lastIdMets',1)");
    }


    if (isset($_POST['sansgluten']))
    {
        $bdd->query("INSERT INTO avoir (METS_ID, CRI_ID) VALUES ('$lastIdMets',2)");
    }

    if (isset($_POST['vegan']))
    {
        $bdd->query("INSERT INTO avoir (METS_ID, CRI_ID) VALUES ('$lastIdMets',3)");
    }


    if (isset($_POST['vegetarien']))
    {
        $bdd->query("INSERT INTO avoir (METS_ID, CRI_ID) VALUES ('$lastIdMets',4)");
    }


    if (isset($_POST['pescetarien']))
    {
        $bdd->query("INSERT INTO avoir (METS_ID, CRI_ID) VALUES ('$lastIdMets',5)");
    }


    //GENRE//

    if (isset($_POST['sucre']))
    {
        $bdd->query("INSERT INTO avoir (METS_ID, CRI_ID) VALUES ('$lastIdMets',6)");
    }


    if (isset($_POST['sale']))
    {
        $bdd->query("INSERT INTO avoir (METS_ID, CRI_ID) VALUES ('$lastIdMets',7)");
    }


    //QUANTITE//

    if (isset($_POST['aperitif']))
    {
        $bdd->query("INSERT INTO avoir (METS_ID, CRI_ID) VALUES ('$lastIdMets',8)");
    }


    if (isset($_POST['atable']))
    {
        $bdd->query("INSERT INTO avoir (METS_ID, CRI_ID) VALUES ('$lastIdMets',9)");
    }

    if (isset($_POST['dessert']))
    {
        $bdd->query("INSERT INTO avoir (METS_ID, CRI_ID) VALUES ('$lastIdMets',10)");
    }

    ?>
    <script type="text/javascript">
        alert("Mets ajouté");
        document.location.href = "indexAdmin.php";
    </script>
    <?php


	$bdd = null;
	
}
 catch (PDOException $e) {
   echo "Erreur !: " . $e->getMessage() . "<br />";
   die();
 }

}

?>

<!DOCTYPE html>

<html class="#000000 black ">
<head>
    <meta charset="utf-8"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="styleM.css" type="text/css" rel="stylesheet"/>
    <title>Ajouter un mets</title>
</head>

<body style ="color: white;">

<?php include('includes/headerAdmin.php'); ?>


<div class="container">
	<div>
		<h5 class="left-align amber-text">Ajouter un mets</h5>
	</div>
<div class="row">

	<form method="post">
		
		<div class="input-field col s12">
			<input class="white-text" id="nom" type="text" name="nom" class="validate" required>
			<label for="nom" class="white-text">nom</label>
		</div>
            
		<table  class="white-text" class="responsive-table">
				<thead>
					<tr>
						<th>Régime alimentaire</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<p class="white-text center">
								<label class="white-text" id="label">
									<input type="checkbox" name="sanslactose" id="input" class="white-text" />
									<span> sans lactose</span>
								</label>
							</p>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>
							<p class="white-text center">
								<label class="white-text">
									<input type="checkbox" name="sansgluten" />
									<span> sans gluten<label class="black-text">x</label></span>
								</label>
							</p>
						</td>
						<td></td>
					</tr>

					<tr>
						<td>
							<p class="white-text center">
								<label class="white-text">
									<input type="checkbox" name="vegan" />
									<span> vegan<label class="black-text">xxxxxxx</label></span>
								</label>
							</p>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>
							<p class="white-text center">
								<label class="white-text ">
									<input type="checkbox" name="vegetarien"/>
									<span> végétarien<label class="black-text">xx</label></span>
								</label>
							</p>
						</td>
					</tr>
					<tr>
						<td class="white-text center">
							<p class="white-text ">
								<label class="white-text">
									<input type="checkbox" name="pescetarien"/>
									<span> pescetarien<label class="black-text">x</label></span>
								</label>
							</p>
						</td>
					</tr>
				</tbody>
				<thead>
					<tr>
						<th>Genre</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="white-text center">
							<p class="white-text  ">
								<label class="white-text">
									<input type="checkbox" name="sucre" />
									<span>sucré<label class="black-text">xxxxxxx</label></span>
								</label>
							</p>
						</td>
					</tr>
					<tr>
						<td class="white-text center">
							<p class="white-text ">
								<label class="white-text">
									<input type="checkbox" name="sale"/>
									<span>salé<label class="black-text">xxxxxxxx</label></span>
								</label>
							</p>
						</td>
					</tr>
				</tbody>
				<thead>
					<tr>
						<th>Quantité</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="white-text center">
							<p class="white-text  ">
								<label class="white-text">
									<input type="checkbox" name="aperitif"/>
									<span>apéritif<label class="black-text">xxxx</label></span>
								</label>
							</p>
						</td>
					</tr>
					<tr>
						<td class="white-text center">
							<p class="white-text ">
								<label class="white-text">
									<input type="checkbox" name="atable"/>
									<span>à table<label class="black-text">xxxx</label></span>
								</label>
							</p>
						</td>
					</tr>
                    <tr>
                        <td class="white-text center">
                            <p class="white-text ">
                                <label class="white-text">
                                    <input type="checkbox" name="dessert"/>
                                    <span>dessert<label class="black-text">xxx</label></span>
                                </label>
                            </p>
                        </td>
                    </tr>
				</tbody>
		</table>


        <div class="input-field col s12">
            <input class="white-text" id="img" type="text" name="img" class="validate" required value="mets/">
            <label for="img" class="white-text">lien image</label>
        </div>

		<input class="btn waves-effect waves-light background white black-text right" name="submit" type="submit" value="Ajouter"  />
		

	</form>





</div>
</div>



<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>
<script type="text/javascript">

  $('.dropdown-trigger').dropdown();

</script>


  


</body>
</html>