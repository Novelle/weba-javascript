<?php 
session_start(); 
require_once 'includes/check.php';

if($_SESSION['CLI_ISADMIN'] != 1)
{
   session_destroy();
   header('Location: seConnecter.php');
   exit();
}
 $nomMets = $_POST['nomMets'];
 $nomUrlImage = $_POST['nomUrlImage'];	

?>

<!DOCTYPE html>

<html class="#000000 black ">
<head>
    <meta charset="utf-8"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="styleM.css" type="text/css" rel="stylesheet"/>
    <title>Page administrative</title>
</head>

<body>
    <?php include('includes/headerAdmin.php'); ?>
  		

    <h1 class="center">Page administrative</h1>
  

<!-- Trigger/Open The Modal -->
<button style="width:130px"id="myBtn">Ajouter un mets</button>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
	<div class="modal-header">
    <span class="close">&times;</span>
    <h3 >Entrer un mets</h3>
  </div>
	<form class="col s12" method="POST" action="indexAdmin.php">
					  <div class="row">
						<div class="input-field col s6" id="inputNomMets"></div>
					  </div>
					  <div class="row">
						<div class="input-field col s6" id="inputUrlMets"></div>
	  				  </div>
	  <input type="submit" id="btnAjouter" style="width:130px" value="Ajouter un mets"/>
	 </form>
    
  </div>

</div>
  
  <script>
// Get the modal
	var nomMets = document.createElement("INPUT");
	var lblMets = document.createElement("LABEL");
  
  	var nomUrlImage = document.createElement("INPUT");
	var lblUrlImage = document.createElement("LABEL");
  
  	var btnAjouter = document.createElement("BUTTON");
  
  	var divNomMets = document.getElementById("inputNomMets");
    var divUrlImage = document.getElementById("inputUrlMets");
  	var divBtnAjouter = document.getElementById("btnAjouter");
  
  	//attributs input nomMets
    nomMets.setAttribute("type", "text");
    nomMets.setAttribute("class", "validate");
    nomMets.setAttribute("id", "nomMets");
    nomMets.setAttribute("required", "");
    nomMets.setAttribute("name", "nomMets");
	
	lblMets.setAttribute("for","nomMets");
	lblMets.innerHTML = "nom Mets";
  
  	nomUrlImage.setAttribute("type", "text");
    nomUrlImage.setAttribute("class", "validate");
    nomUrlImage.setAttribute("id", "nomUrlImage");
    nomUrlImage.setAttribute("required", "");
    nomUrlImage.setAttribute("name", "nomUrlImage");
  
	lblUrlImage.setAttribute("for","nomUrlImage");
	lblUrlImage.innerHTML = "Url du Mets";
  
  //attributs btn Envoyer
    btnAjouter.setAttribute("type", "submit");
    btnAjouter.setAttribute("class", "btn waves-effect waves-light");
	btnAjouter.innerHTML = "Ajouter mets";
  
  	divNomMets.appendChild(lblMets);
	divNomMets.appendChild(nomMets);
	
	divUrlImage.appendChild(lblUrlImage);
	divUrlImage.appendChild(nomUrlImage);
	
  
  
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}



//When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

btnAjouter.onclick = function() 
{
  <?php
  try 
  {

	$bdd = new PDO("mysql:host=hhva.myd.infomaniak.com;dbname=hhva_ninotchkanv", "hhva_ninotchkanv", "dt14q9PS3v");

	$bdd->query("SET NAMES 'utf8'");
	

	$bdd->query("INSERT INTO mets (METS_NOM, METS_LIENIMAGE) VALUES ('$nomMets', '$nomUrlImage')");


    $lastIdMets = $bdd->lastInsertId();


	$bdd = null;
	
  }
 catch (PDOException $e) 
 {
   echo "Erreur !: " . $e->getMessage() . "<br />";
   die();
 }
?>
}

//fonction qui fait appel à une autre fonction
document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.sidenav');
      var instances = M.Sidenav.init(elems, options);
    });

    $(document).ready(function(){
  $('.modal').modal();
});
</script>
    <br/>
    <div class="image" style="text-align:center">
        <img src="imageCollection/imageAdmin.jpeg" class="responsive-img" width="1000" align="center" alt="Photo de collection" />
    </div>

    <div class="row center">
        <div class="row">
            <div class="input-field col s12">
                <a class="amber-text" href =listeClient.php>Déscativer un client</a></br>
		   		<a class="amber-text" href =suggestion.php>Valider / Supprimer une suggestion</a>
            </div>
        </div>
    </div>
  
  
  
  
	

<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>
<script type="text/javascript">

    $('.dropdown-trigger').dropdown();

    $(document).ready(function(){
        $('select').formSelect();
    });
  
  /*	$(document).ready(function()
	{
  		$('.modal').modal();
	});*/

</script>


</body>
</html>
