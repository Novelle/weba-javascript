<head>
    <meta charset="utf-8"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="styleM.css" type="text/css" rel="stylesheet"/>
    <title>En-tête</title>
</head>

<body style ="color: white;">
<header>
    <nav class="black">

        <div class="row">   </div>

        <div class="container">

            <div class="nav-wrapper">

                <a href="indexAdmin.php" class="brand-logo">
                    <img src="imageCollection/logo.jpg" class="responsive-img"  width=100 height=100  alt="Photo du logo"/>
                </a>
                <a href="#" data-target="mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <div class ="right">

                    <a class='dropdown-trigger btn background white black-text' href='#' data-target='dropdown1'>Ajouter</a>
                    <ul id='dropdown1' class='dropdown-content'>
                        <li><a href="ajouterVin.php">Vin</a></li>
                        <li><a href="ajouterMets.php">Mets</a></li>
                    </ul>

                    <a class='dropdown-trigger btn background white black-text' href='#' data-target='dropdown2'>Modifier</a>
                    <ul id='dropdown2' class='dropdown-content'>
                        <li><a href="modifierVin.php">Vin</a></li>
                        <li><a href="modifierMets.php">Mets</a></li>
                        <li><a href="modifierAccord.php">Accords</a></li>
                    </ul>

                    <a class='dropdown-trigger btn background white black-text' href='#' data-target='dropdown3'>Supprimer</a>
                    <ul id='dropdown3' class='dropdown-content'>
                        <li><a href="supprimerVin.php">Vin</a></li>
                        <li><a href="supprimerMets.php">Mets</a></li>
                        <li><a href="supprimerAccord.php">Accords</a></li>
                    </ul>

                    <ul class="right">
                        <li><a href="suggestion.php">Suggestions</a></li>
                        <li><a href="listeClient.php">Clients</a></li>
                        <li><a href="deconnexion.php">Deconnexion</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </nav>

    <ul class="sidenav" id="mobile">
        <li><a href="ajouterVin.php">Ajouter vin</a></li>
        <li><a href="ajouterMets.php">Ajouter mets</a></li>
        <li><a href="modifierVin.php">Modifier vin</a></li>
        <li><a href="modifierMets.php">Modifier mets</a></li>
        <li><a href="modifierAccord.php">Modifier accords</a></li>
        <li><a href="supprimerVin.php">Supprimer vin</a></li>
        <li><a href="supprimerMets.php">Supprimer mets</a></li>
        <li><a href="supprimerAccord.php">Supprimer accords</a></li>
        <li><a href="suggestion.php">Suggestions</a></li>
        <li><a href="listeClient.php">Clients</a></li>
    </ul>
</header>

<br/><br/><br/><br/>


