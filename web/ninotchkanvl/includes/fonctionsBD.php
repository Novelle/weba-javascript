<?php
session_start();
/**
 * Created by PhpStorm.
 * User: KARA_NISA-ESIG
 * Date: 12.10.2018
 * Time: 14:02
 */
define('DB_HOST', "hhva.myd.infomaniak.com"); //adresse ip de la base.
define('DB_NAME', "hhva_ninotchkanv"); //nom de la base de donnée.
define('DB_USER', "hhva_ninotchkanv"); //utilisateur.
define('DB_PASS', "dt14q9PS3v"); //mdp.
/* Fonction permettant la connexion à la base de données  */
function getConnexion()
{
    static $dbb = null;
    if ($dbb === null) {
        try {
            $connectionString = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '';
            $dbb = new PDO($connectionString, DB_USER, DB_PASS);
            $dbb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $dbb->exec("SET CHARACTER SET utf8");
        } catch (PDOException $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }
    return $dbb;
}

function selectUserInfo($mail)
{
    $connexion = getConnexion();
    $request = $connexion->prepare("SELECT CLI_ID, CLI_LOGIN_MAIL, CLI_MDP FROM client WHERE CLI_LOGIN_MAIL = :mail;");
    $request->bindParam(':mail', $mail, PDO::PARAM_STR);
    $request->execute();
    return $request->fetchAll(PDO::FETCH_ASSOC);
}

function login($mail, $password)
{
    $connexion = getConnexion();
    $request = $connexion->prepare("SELECT CLI_ID, CLI_LOGIN_MAIL, CLI_MDP FROM client WHERE CLI_LOGIN_MAIL = :mail AND CLI_MDP = :password;");
    $request->bindParam(':mail', $mail, PDO::PARAM_STR);
    $request->bindParam(':password', $password, PDO::PARAM_STR);
    $request->execute();
  	if(CLI_MDP != $password)
	  {
	  	  ?><script type="text/javascript">alert("Mauvais information de connexion !");</script><?php
            echo "Mauvaise informations de connexion !!";
	  }

    $nbRequest = $request->rowCount();
    return $request->fetchAll(PDO::FETCH_ASSOC);
}


function getMetsAcoords($id)
{
    try {
        $request = getConnexion()->prepare("  SELECT METS_NOM FROM METS, SUGGESTION WHERE METS.METS_ID = SUGGESTION.METS_ID AND SUGGESTION.SUG_ID = :id;");
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll();
    } catch (PDOException $e) {
        throw $e;
    }
}

function getVinAcoords($id)
{
    try {
        $request = getConnexion()->prepare(" SELECT VIN_NOM FROM VIN, SUGGESTION WHERE VIN.VIN_ID = SUGGESTION.VIN_ID AND SUGGESTION.SUG_ID = :id;");
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll();
    } catch (PDOException $e) {
        throw $e;
    }
}


function selectVin($nom){
    $connexion = getConnexion();
    $request = $connexion->prepare("SELECT `VIN_ID` FROM vin WHERE `VIN_NOM` = :nom");
    $request->bindParam(':nom', $nom, PDO::PARAM_STR);
    $request->execute();
    return $request->fetch();
}

function addMets($nom, $nbRecherche){
    $request = getConnexion()->prepare("INSERT INTO mets (`METS_NOM`, `METS_NBRECHERCHE`) VALUES (:nom, :nbRecherche)");
    $request->bindParam(':nom', $nom, PDO::PARAM_STR);
    $request->bindParam(':nbRecherche', $nbRecherche, PDO::PARAM_STR);
    $request->execute();
    return getConnexion()->lastInsertId();
}

function addSuggestion($idClient, $idVin, $idMets, $etat){
  if($idClient == 11)
  {
	$etat = 0;
	$request = getConnexion()->prepare("INSERT INTO suggestion (`CLI_ID`, `VIN_ID`, `METS_ID`, `SUG_ETATSUGGESTION`) VALUES (:idClient, :idVin, :idMets, :etat)");
    $request->bindParam(':idClient', $idClient, PDO::PARAM_STR);
    $request->bindParam(':idVin', $idVin, PDO::PARAM_STR);
    $request->bindParam(':idMets', $idMets, PDO::PARAM_STR);
    $request->bindParam(':etat', $etat, PDO::PARAM_STR);
    $request->execute();
  }
  else
  {
  	$request = getConnexion()->prepare("INSERT INTO suggestion (`CLI_ID`, `VIN_ID`, `METS_ID`, `SUG_ETATSUGGESTION`) VALUES (:idClient, :idVin, :idMets, :etat)");
    $request->bindParam(':idClient', $idClient, PDO::PARAM_STR);
    $request->bindParam(':idVin', $idVin, PDO::PARAM_STR);
    $request->bindParam(':idMets', $idMets, PDO::PARAM_STR);
    $request->bindParam(':etat', $etat, PDO::PARAM_STR);
    $request->execute();
  }	
    
}

//Ajouter mets
function createMets($nomMets)
{
    try {
        $connexion = getConnexion();
        $request = $connexion->prepare("INSERT INTO METS (MET_NOM) VALUES (':nomMets')");
        $request->bindParam(':nom', $nomMets, PDO::PARAM_STR);
        $request->execute();
    } catch (PDOException $e) {
        throw $e;
    }
}

//

function getClient($id)
{
    try {
        $request = getConnexion()->prepare("SELECT * FROM client WHERE CLI_ID = :id");
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();

        return $request->fetch();
    } catch (PDOException $e) {
        throw $e;
    }
}

//

function getVin($id)
{
    try {
        $request = getConnexion()->prepare("SELECT * FROM vin WHERE VIN_ID = :id");
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();

        return $request->fetch();
    } catch (PDOException $e) {
        throw $e;
    }
}

//

function getMets($id)
{
    try {
        $request = getConnexion()->prepare("SELECT * FROM mets WHERE METS_ID = :id");
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();

        return $request->fetch();
    } catch (PDOException $e) {
        throw $e;
    }
}

function getSuggestion($id)
{
    try {
        $request = getConnexion()->prepare("SELECT * FROM `SUGGESTION` WHERE SUG_ID = :id");
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();

        return $request->fetch();
    } catch (PDOException $e) {
        throw $e;
    }
}



function updateUser($id, $nom, $prenom, $etatClient, $mail)
{
    try {
        getConnexion()->query("UPDATE client SET CLI_NOM = '$nom', CLI_PRENOM = '$prenom', `CLI_ETATCLIENT` = $etatClient, `CLI_LOGIN_MAIL` = '$mail' WHERE CLI_ID = $id");
  
    } catch (PDOException $e) {
        throw $e;
    }
}


function deleteMets($id)
{
    try {
        $request = getConnexion()->prepare("DELETE FROM `METS` WHERE METS_ID = :id");
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();
    } catch (PDOException $e) {
        throw $e;
    }
}







// Modification mets

function updateMets($id, $nom, $img)
{
    try {
        getConnexion()->query("UPDATE mets SET METS_NOM = '$nom', METS_LIENIMAGE = '$img' WHERE METS_ID = $id");
    } catch (PDOException $e) {
        throw $e;
    }
}

function getMetsRegime($id)
{
    try {
        $request = getConnexion()->prepare("  SELECT criteremets.cri_nom, mets.METS_NOM 
                                                        FROM mets,criteremets, avoir
                                                        WHERE mets.METS_ID = avoir.METS_ID
                                                        AND criteremets.CRI_ID = avoir.CRI_ID
                                                        AND mets.METS_ID = :id");
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll();
    } catch (PDOException $e) {
        throw $e;
    }
}

function deleteRegime($id)
{
    try {
        $request = getConnexion()->prepare("DELETE FROM avoir WHERE METS_ID = :id");
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();
    } catch (PDOException $e) {
        throw $e;
    }
}

function insertRegime($idMet, $idCri)
{
    try {
        $connexion = getConnexion();
        $request = $connexion->prepare("INSERT INTO avoir (METS_ID,CRI_ID) VALUES ($idMet,$idCri)");
        $request->execute();
    } catch (PDOException $e) {
        throw $e;
    }
}

// Modification vin

function updateVin($id, $nom, $millesime, $couleur, $collection, $ptgarde, $prix, $tpservice, $flaconnage, $degreealcool, $img )
{
    try {
        getConnexion()->query("UPDATE vin SET VIN_NOM = '$nom', VIN_MILLESIME = '$millesime', VIN_COULEUR = '$couleur', VIN_COLLECTION = '$collection',
        VIN_POTENTIELGARDE = '$ptgarde', VIN_PRIX = '$prix', VIN_TEMPERATURESERVICE = '$tpservice', VIN_FLACONNAGE = '$flaconnage', VIN_DEGREALCOOL = '$degreealcool', VIN_LIENIMAGE = '$img' WHERE VIN_ID = $id");
    } catch (PDOException $e) {
        throw $e;
    }
}

function getVinCepage($id)
{
    try {
        $request = getConnexion()->prepare("  SELECT cepage.cep_nom, vin.VIN_NOM 
                                                        FROM vin,cepage, contenir
                                                        WHERE vin.VIN_ID = contenir.VIN_ID
                                                        AND cepage.CEP_ID = contenir.CEP_ID
                                                        AND vin.VIN_ID = :id");
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll();
    } catch (PDOException $e) {
        throw $e;
    }
}

function deleteCepage($id)
{
    try {
        $request = getConnexion()->prepare("DELETE FROM contenir WHERE VIN_ID = :id");
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();
    } catch (PDOException $e) {
        throw $e;
    }
}

function insertCepage($idVin, $idCep)
{
    try {
        $connexion = getConnexion();
        $request = $connexion->prepare("INSERT INTO contenir(VIN_ID,CEP_ID) VALUES ($idVin,$idCep)");
        $request->execute();
    } catch (PDOException $e) {
        throw $e;
    }
}

function getMetsAccord($id)
{
    try {
        $request = getConnexion()->prepare(" SELECT FROM `METS` WHERE VIN_ID = :id");
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll();
    } catch (PDOException $e) {
        throw $e;
    }
}


//

//

function getMetsAccords($id)
{
    try {
        $request = getConnexion()->prepare("SELECT METS_NOM FROM mets, suggestion WHERE mets.METS_ID = suggestion.METS_ID AND suggestion.SUG_ID = :id");
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();

        $donnees = $request->fetch();
        return $donnees['METS_NOM'];
    } catch (PDOException $e) {
        throw $e;
    }
}


function getVinAccord($id)
{
    try {
        $request = getConnexion()->prepare(" SELECT VIN_NOM FROM vin, suggestion WHERE vin.VIN_ID = suggestion.VIN_ID AND suggestion.SUG_ID = :id");
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();

        $donnees = $request->fetch();
        return $donnees['VIN_NOM'];
    } catch (PDOException $e) {
        throw $e;
    }
}









?>

